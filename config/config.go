package config

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"git.giaever.org/joachimmg/go-log.git/errors"
)

type LogPrefix string
type LogLevel int
type LogHandle io.Writer
type LogFlag int
type LogDir string

var (
	// DiscardHandle: used when log level isnt reached (discard log)
	DiscardHandle LogHandle = ioutil.Discard
	// HandleTrace: output dest for trace log
	HandleTrace LogHandle = os.Stdout
	// HandleInfo: output dest for info log
	HandleInfo LogHandle = os.Stdout
	// HandleWarning: output dest for warning log
	HandleWarning LogHandle = os.Stdout
	// HandleError: output dest for error log
	HandleError LogHandle = os.Stdout
	// HandlePanic: output dest for panic log
	HandlePanic LogHandle = os.Stderr
	// Ldir: Log directory, where file (not "output") logs are stored.
	Ldir LogDir = LogDir(os.TempDir() + string(os.PathSeparator) + "go-log")
)

const (
	// LogFormat: format in front of log text
	LogFormat = log.Ldate | log.Lmicroseconds
	// FileMask: filedescriptor mask
	FileMask = os.O_CREATE | os.O_WRONLY | os.O_APPEND
	// FilePerm: File permission, 0600
	FilePerm = 0600
)

const (
	// PrefixTrace: Log prefix for trace
	PrefixTrace LogPrefix = "TRACE"
	// PrefixInfo: Log prefix for info
	PrefixInfo LogPrefix = "INFO"
	// PrefixWarning: Log prefix for warning
	PrefixWarning LogPrefix = "WARNING"
	// PrefixError: Log prefix for error
	PrefixError LogPrefix = "ERROR"
	// PrefixPanic: Log prefix for panic
	PrefixPanic LogPrefix = "PANIC"
)

const (
	// Trace level
	Trace LogLevel = 1<<iota | 0
	// Info level
	Info LogLevel = 1<<iota | Trace
	// Warning level
	Warning LogLevel = 1<<iota | Info
	// Error level
	Error LogLevel = 1<<iota | Warning
	// Panic level
	Panic LogLevel = 1<<iota | Error
)

var (
	// Llo (log level output)
	Llo LogLevel = Info
	// Llf (log level file)
	Llf LogLevel = Warning
)

// Set log level
func (c *LogLevel) Set(l string) error {

	l = strings.ToUpper(l)

	if l == "TRACE" {
		*c = Trace
	} else if l == "INFO" {
		*c = Info
	} else if l == "WARNING" {
		*c = Warning
	} else if l == "ERROR" {
		*c = Error
	} else if l == "PANIC" {
		*c = Panic
	} else {
		return errors.UnknownLevel
	}

	return nil
}

// String function to print log level
func (c *LogLevel) String() string {
	switch *c {
	case Trace:
		return "TRACE"
	case Info:
		return "INFO"
	case Warning:
		return "WARNING"
	case Error:
		return "ERROR"
	default:
		return "PANIC"
	}
}

// Set logging directory
func (d *LogDir) Set(nd string) error {
	Ldir = LogDir(nd)
	return nil
}

// String function to print log level
func (d *LogDir) String() string {
	return string(*d)
}
