package errors

import (
	e "errors"
)

var (
	UnknownLogger = e.New("Unknown logger")
	UnknownLevel  = e.New("Unknown log level: TRACE|INFO|WARNING|ERROR|PANIC")

	InvalidPath = e.New("Invalid path")
)
