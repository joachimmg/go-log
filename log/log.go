package log

import (
	"flag"
	"fmt"
	"io"
	_log "log"
	"os"
	"strings"

	"git.giaever.org/joachimmg/go-log.git/config"
	"git.giaever.org/joachimmg/go-log.git/errors"
)

// Init flags
func init() {
	flag.Var(&config.Llo, "llo", "Minimum log level that will be written to stdout.")
	flag.Var(&config.Llf, "llf", "Minimum log level that will be written to file")
	flag.Var(&config.Ldir, "ldir", "Directory to log to")
}

// Loggers
var (
	logTrace   *_log.Logger
	logInfo    *_log.Logger
	logWarning *_log.Logger
	logError   *_log.Logger
	logPanic   *_log.Logger
)

// Create log-file in director: Also creates the directory tree.
func log_file(p config.LogPrefix) *os.File {
	os.MkdirAll(config.Ldir.String(), 0777)
	f, err := os.OpenFile(
		fmt.Sprintf("%s%c%s.log", config.Ldir, os.PathSeparator, strings.ToLower(string(p))),
		config.FileMask,
		config.FilePerm,
	)

	if err != nil {
		_log.Fatal(err.Error())
		return nil
	}

	return f
}

// Create new logger
func log_new(l config.LogLevel, h config.LogHandle, p config.LogPrefix) *_log.Logger {

	if (config.Llf&l) == config.Llf && (config.Llo&l) == config.Llo {
		f := log_file(p)
		return _log.New(
			io.MultiWriter(f, h),
			fmt.Sprintf("%s ", p),
			config.LogFormat,
		)
	}

	if (config.Llf & l) == config.Llf {
		f := log_file(p)
		return _log.New(
			f, fmt.Sprintf("%s ", p),
			config.LogFormat,
		)
	}

	if (config.Llo & l) == config.Llo {
		return _log.New(
			h, fmt.Sprintf("%s ", p),
			config.LogFormat,
		)
	}

	return _log.New(
		config.DiscardHandle, fmt.Sprintf("%s ", p),
		config.LogFormat,
	)
}

// Internal log function based on log level
func log(t config.LogLevel) *_log.Logger {
	var l *_log.Logger
	switch t {
	case config.Trace:
		if logTrace == nil {
			logTrace = log_new(t, config.HandleTrace, config.PrefixTrace)
		}
		l = logTrace
	case config.Info:
		if logInfo == nil {
			logInfo = log_new(t, config.HandleInfo, config.PrefixInfo)
		}
		l = logInfo
	case config.Warning:
		if logWarning == nil {
			logWarning = log_new(t, config.HandleWarning, config.PrefixWarning)
		}
		l = logWarning
	case config.Error:
		if logError == nil {
			logError = log_new(t, config.HandleError, config.PrefixError)
		}
		l = logError
	case config.Panic:
		if logPanic == nil {
			logPanic = log_new(t, config.HandlePanic, config.PrefixPanic)
		}
		l = logPanic
	}

	if l == nil {
		_log.Panic(errors.UnknownLogger.Error())
	}

	return l
}

// Llo returns output log level config
func Llo() *config.LogLevel {
	return &config.Llo
}

// Llf return file log level config
func Llf() *config.LogLevel {
	return &config.Llf
}

// Ldir return log directory config
func Ldir() *config.LogDir {
	return &config.Ldir
}

// Trace
func Trace(m string) {
	log(config.Trace).Print(m)
}

// Tracef: format
func Tracef(f string, v ...interface{}) {
	log(config.Trace).Printf(f, v...)
}

// Traceln: format standard
func Traceln(v ...interface{}) {
	log(config.Trace).Println(v...)
}

// Info
func Info(m string) {
	log(config.Info).Print(m)
}

// Infof: format
func Infof(f string, v ...interface{}) {
	log(config.Info).Printf(f, v...)
}

// Info: format standard
func Infoln(v ...interface{}) {
	log(config.Info).Println(v...)
}

// Warning
func Warning(m string) {
	log(config.Warning).Print(m)
}

// Warningf: format
func Warningf(f string, v ...interface{}) {
	log(config.Warning).Printf(f, v...)
}

// Warningln: format standard
func Warningln(v ...interface{}) {
	log(config.Warning).Println(v...)
}

// Error
func Error(m string) {
	log(config.Error).Fatal(m)
}

// Errorf: format
func Errorf(f string, v ...interface{}) {
	log(config.Error).Fatalf(f, v...)
}

// Errorln: format standard
func Errorln(v ...interface{}) {
	log(config.Error).Fatalln(v...)
}

// Panic
func Panic(m string) {
	log(config.Panic).Panic(m)
}

// Panicf: format
func Panicf(f string, v ...interface{}) {
	log(config.Panic).Panicf(f, v...)
}

// Panicln: format standard
func Panicln(v ...interface{}) {
	log(config.Panic).Panicln(v...)
}
