# go-log

Make logging a charm and log whatever you want, whenever you want.

## Simple usage
Just include the package `git.giaever.org/joachimmg/go-log.git/log` in the top of your file and start your logging, e.g:
```go
log.Tracef("This is a %s-log", "trace")
log.Infof("This is a %s-log", "info")
log.Warningf("This is a %s-log", "warning")
log.Errorf("This is a %s-log", "error")
log.Panicf("This is a %s-log", "panic")
```

which gives you an output like
```
TRACE 2016/11/25 01:21:24.243863 This is a trace-log
INFO 2016/11/25 01:21:24.243993 This is a info-log
WARNING 2016/11/25 01:21:24.244005 This is a warning-log
ERROR 2016/11/25 01:21:24.244023 This is a error-log
```

Yes, Panic wont appear here as Error and Panic functions will end the program!

## How to adjust output level?
Well, you can parse the flags, e.g by importing flags into your application where `log` is used. E.g:

```go
import (
    "flag"

    "git.giaever.org/joachimmg/go-log.git/log"
)

func main() {
    flag.Parse()
    log.Trace("Will be shown if flag -llo TRACE is set and be stored into file if -llf TRACE is set")
}
```

### Flags?
Yes, now you can specify to your program what log-level you want

1. Printed to screen: `-llo` (log level output)
2. Stored to file: `-llf` (log level file)

Example: `./my_app -llo INFO -llf TRACE" will print info, warning, error and panic logs, but not trace. Everyone (including trace logs) will be written to file.

### Nah, I dont want flags in my application.
Okey, set the variables manually by including the ./config, and call `config.LLO.Set(config.INFO)` or `config.LLF.Set(config.TRACE)`.

Enjoy!
