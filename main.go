package main

import (
	"flag"

	"git.giaever.org/joachimmg/go-log.git/log"
)

func main() {
	flag.Parse()
	log.Tracef("This is a %s-log", "trace")
	log.Infof("This is a %s-log", "info")
	log.Warningf("This is a %s-log", "warning")
	log.Errorf("This is a %s-log", "error")
	log.Panicf("This is a %s-log", "panic")
}
